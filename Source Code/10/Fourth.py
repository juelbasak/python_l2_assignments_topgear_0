from Second import Second
from Third import Third
from First import First

class Fourth(Second, Third):
    def method1(self):
        return 'This is Fourth Class'



obj = Fourth()

print(obj.method1())
print(super(Fourth, obj).method1())
print(super(Third, obj).method1())
print(super(Second, obj).method1())

print('Fourth class MRO -> ', Fourth.mro())
print('Third class MRO -> ', Third.mro())
print('Second class MRO -> ', Second.mro())
print('First class MRO -> ', First.mro())
