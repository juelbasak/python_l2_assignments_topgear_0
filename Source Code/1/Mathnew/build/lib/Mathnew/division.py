def division(num1, num2):
    try:
        return num1 / num2
    except ZeroDivisionError:
        return 'Division by Zero encountered'