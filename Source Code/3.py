class Mymath:
    def sqroot(num):
        return num ** 0.5

    def addition(num1, num2):
        return num1 + num2

    def subtraction(num1, num2):
        return num1 - num2

    def multiplication(num1, num2):
        return num1 * num2

    def division(num1, num2):
        try:
            return num1 / num2
        except ZeroDivisionError:
            return 'Cannot Divide by Zero'


get_num1 = float(input('Enter a number -> '))
get_num2 = float(input('Enter another number -> '))

print(f'Square Root of {get_num1} is {Mymath.sqroot(get_num1)}')
print('Addition result is : ', Mymath.addition(get_num1, get_num2))
print('Subtraction result is : ', Mymath.subtraction(get_num1, get_num2))
print('Multiplication result is : ', Mymath.multiplication(get_num1, get_num2))
print('Division result is : ', Mymath.division(get_num1, get_num2))
