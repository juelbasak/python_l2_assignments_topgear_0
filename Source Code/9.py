from math import pi

class Circle:
    def __init__(self, radius):
        self.radius = radius

    def area(self):
        return pi*(self.radius**2)
    
    def circumference(self):
        return 2 * pi * self.radius

    def __str__(self):
        return f'Radius is : {self.radius} units'

    
c1 = Circle(float(input('Enter the radius -> ')))
print(c1)
print('Area : {0:.2f} unit square.'.format(c1.area()))
print('Circumference : {0:.2f} units.'.format(c1.circumference()))

