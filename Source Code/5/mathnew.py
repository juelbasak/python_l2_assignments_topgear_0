from sqroot import sqroot
from addition import addition
from subtraction import subtraction
from multiplication import multiplication
from division import division

class mathnew(sqroot, addition, subtraction, multiplication, division):
    
    def __init__(self, num1, *args):
        self.num1 = num1
        self.num2 = args[0]


    def calc(self):
        print(f'Square Root of {self.num1} -> {super().square_root()}')
        print(f'Addition -> {super().add()}')
        print(f'Subtraction -> {super().sub()}')
        print(f'Multiplication -> {super().product()}')
        print(f'Division -> {super().divide()}')


num1 = int(input('Enter a number -> '))
num2 = int(input('Enter another number -> '))

obj = mathnew(num1, num2)
obj.calc()

